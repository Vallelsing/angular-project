import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ProductsIndexComponent } from './components/products/products-index/products-index.component';
import { OrdersIndexComponent } from './components/orders/orders-index/orders-index.component';
import { UsersIndexComponent } from './components/users/users-index/users-index.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ProductsShowComponent } from './components/products/products-show/products-show.component';
import { UsersShowComponent } from './components/users/users-show/users-show.component';
import { OrdersShowComponent } from './components/orders/orders-show/orders-show.component';
import { ContactComponent } from './components/contact/contact.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductsIndexComponent,
    OrdersIndexComponent,
    UsersIndexComponent,
    NavbarComponent,
    ProductsShowComponent,
    UsersShowComponent,
    OrdersShowComponent,
    ContactComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
