import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ProductService} from "../../../services/product.service";

@Component({
  selector: 'app-products-show',
  templateUrl: './products-show.component.html',
  styleUrls: ['./products-show.component.css']
})
export class ProductsShowComponent implements OnInit {

  product: any;

  constructor(private route: ActivatedRoute, private productService: ProductService ) { }

  ngOnInit(): void {
    this.product = this.productService.getProduct(this.route.snapshot.paramMap.get('id'));
  }

}
