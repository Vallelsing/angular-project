import {Component, OnInit} from '@angular/core';
import {ProductService} from "../../../services/product.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-products-index',
  templateUrl: './products-index.component.html',
  styleUrls: ['./products-index.component.css']
})
export class ProductsIndexComponent implements OnInit {

  products: any;

  constructor(private productService: ProductService, private router: Router) {
    this.products = this.productService.getAllProducts();
  }

  ngOnInit(): void {
  }

  redirect(id : number) : void {
    this.router.navigate(['/products/' + id]);
  }
}
