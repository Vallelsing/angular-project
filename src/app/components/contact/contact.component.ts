import { Component, OnInit } from '@angular/core';
import { FormBuilder } from "@angular/forms";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  form = this.formBuilder.group({
    name: '',
    email: '',
    content: ''
  });

  showModal : boolean = false;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
  }

  toggleModal(): void {
    if (this.showModal) {
      this.showModal = !this.showModal;
      this.form.controls['name'].setValue('');
      this.form.controls['email'].setValue('');
      this.form.controls['content'].setValue('');
    } else {
        if (this.form.controls["name"].value != ''
          && this.form.controls["email"].value != ''
          && this.form.controls["content"].value != '') {
          this.showModal = !this.showModal;
        }
    }
  }
}
