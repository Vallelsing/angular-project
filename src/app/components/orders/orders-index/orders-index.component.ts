import { Component, OnInit } from '@angular/core';
import { OrderService } from "../../../services/order.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-orders-index',
  templateUrl: './orders-index.component.html',
  styleUrls: ['./orders-index.component.css']
})
export class OrdersIndexComponent implements OnInit {

  orders: any;

  constructor(private orderService: OrderService, private router: Router) {
    this.orders = this.orderService.getAllOrders();
  }

  ngOnInit(): void {
  }

  redirect(id : number) : void {
    this.router.navigate(['/orders/' + id]);
  }
}
