import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {OrderService} from "../../../services/order.service";
import {UserService} from "../../../services/user.service";
import {ProductService} from "../../../services/product.service";

@Component({
  selector: 'app-orders-show',
  templateUrl: './orders-show.component.html',
  styleUrls: ['./orders-show.component.css']
})
export class OrdersShowComponent implements OnInit {

  order: any;
  user: any;
  product: any;

  constructor(private route: ActivatedRoute,
              private orderService : OrderService,
              private userService: UserService,
              private productService: ProductService) { }

  ngOnInit(): void {
    this.order = this.orderService.getOrder(this.route.snapshot.paramMap.get('id'));
    this.user = this.userService.getUser(this.order.user_id);
    this.product = this.productService.getProduct(this.order.product_id);
  }

}
