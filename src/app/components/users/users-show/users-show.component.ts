import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {UserService} from "../../../services/user.service";

@Component({
  selector: 'app-users-show',
  templateUrl: './users-show.component.html',
  styleUrls: ['./users-show.component.css']
})
export class UsersShowComponent implements OnInit {

  user: any;

  constructor(private route: ActivatedRoute, private userService: UserService ) { }

  ngOnInit(): void {
    this.user = this.userService.getUser(this.route.snapshot.paramMap.get('id'))
  }

}
