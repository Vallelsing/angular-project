import { Component, OnInit } from '@angular/core';
import { UserService } from "../../../services/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-users-index',
  templateUrl: './users-index.component.html',
  styleUrls: ['./users-index.component.css']
})
export class UsersIndexComponent implements OnInit {

  users: any;

  constructor(private userService: UserService, private router: Router) {
    this.users = this.userService.getAllUsers();
  }

  ngOnInit(): void {
  }

  redirect(id : number) : void {
    this.router.navigate(['/users/' + id]);
  }
}
