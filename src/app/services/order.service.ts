import { Injectable } from '@angular/core';
import ordersJSON from "../../assets/orders.json"

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  orders = ordersJSON;

  constructor() { }

  public getAllOrders() {
    return this.orders;
  }

  public getOrder(id: string | null) {
    return this.orders[parseInt(<string>id)-1];
  }
}
