import { Injectable } from '@angular/core';
import usersJSON from "../../assets/users.json"

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private users = usersJSON;

  constructor() { }

  public getAllUsers() {
    return this.users;
  }

  public getUser(id: string | null) {
    return this.users[parseInt(<string>id)-1];
  }
}
