import { Injectable } from '@angular/core';
import productsJSON from "../../assets/products.json"

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private products = productsJSON;

  constructor() {  }

  public getAllProducts() {
    return this.products;
  }

  public getProduct(id: string | null) {
    return this.products[parseInt(<string>id)-1];
  }
}
