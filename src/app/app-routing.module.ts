import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ProductsIndexComponent} from "./components/products/products-index/products-index.component";
import {OrdersIndexComponent} from "./components/orders/orders-index/orders-index.component";
import {UsersIndexComponent} from "./components/users/users-index/users-index.component";
import {ProductsShowComponent} from "./components/products/products-show/products-show.component";
import {UsersShowComponent} from "./components/users/users-show/users-show.component";
import {OrdersShowComponent} from "./components/orders/orders-show/orders-show.component";
import {ContactComponent} from "./components/contact/contact.component";

const routes: Routes = [
  { path: '', redirectTo: '/products', pathMatch: 'full' },
  { path: 'products', component: ProductsIndexComponent },
  { path: 'products/:id', component: ProductsShowComponent },
  { path: 'users', component: UsersIndexComponent},
  { path: 'users/:id', component: UsersShowComponent },
  { path: 'orders', component: OrdersIndexComponent },
  { path: 'orders/:id', component: OrdersShowComponent },
  { path: 'contact', component: ContactComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
